import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuController } from '@ionic/angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  data: Object;
  dataObj: Object;
  group: string;
  lat: any;
  lng: any;
  constructor(
  private storage: Storage,
  private http: HttpClient,
  private router: Router,
  private geolocation: Geolocation,
  private sanitizer: DomSanitizer,
  private menuCtrl: MenuController,
  private speechRecognition: SpeechRecognition,
  public platform: Platform) {

  }

  ngOnInit() {
    if (this.platform.is('cordova')) {
      this.speechRecognition.hasPermission()
          .then((hasPermission: boolean) => {
            if (!hasPermission) {
              this.speechRecognition.requestPermission()
              .then(
                () => console.log('Granted'),
                () => console.log('Denied')
              )
            }
         });
    }
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'loggedIn');

    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.storage.get('group').then((val2) => {
        console.log('Group: ', val2);
        var group = val2;
        var uid = val;
        var lat = 51.47;
        var lng = -2.2;
        console.log(uid+" "+group+" "+lat+" "+lng);
        //this.geolocation.getCurrentPosition().then((resp) => {
        //this.http.post('https://rs.starterup.co.uk/rupert/test/create', { uid: uid, lat: resp.coords.latitude, lng: resp.coords.longitude}).subscribe((data:any) => {
         this.http.post('https://rs.starterup.co.uk/rupert/test/list', { uid: uid, group: group, lat: lat, lng: lng}).subscribe((data:any) => {
            //data has list of nearby experiments
            console.log(data.data);
            this.dataObj = data.data;
            console.log(Object.entries(data.data));
            }, error => {
             console.log(error);
           });
          });
        }
        /*}).catch((error) => {
          console.log('Error getting location', error);
        });*/
    });
  }

  join(mail,nid) {
      console.log('checking to see if somebody has joined');
      this.storage.get('uid').then((val) => {
         this.http.post('https://rs.starterup.co.uk/rupert/test/join', { mail: mail, uid: val, nid: nid}).subscribe((data:any) => {
           if (!data.error) {
             this.storage.set('nid', nid);
             this.storage.set('result_id', data.result_id);
             this.router.navigateByUrl('/experimentee');
             console.log(data);
           } else {
             console.log('error');
             this.router.navigateByUrl('/list');
           }
          }, error => {
           console.log(error);
         });
     });
  }

  back() {
    this.router.navigateByUrl('/home');
  }
}
