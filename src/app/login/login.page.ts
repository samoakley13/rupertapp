import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

declare var cordova;

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {
  constructor(private auth: AuthService, private storage: Storage, private http: HttpClient, private router: Router, private menuCtrl: MenuController) {

  }
  ngOnInit(){
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'loggedOut');
  }

  login(form) {
    console.log(form.form.value)
    var mail = form.form.value.email;
    var name = form.form.value.name;
    var group = form.form.value.group;
    console.log('name: ' + name + " group: " + group);
    this.storage.set('group', group);
    this.auth.login(form.value).subscribe((res)=>{
      this.router.navigateByUrl('/home');
    });
    // this.http.post('https://rs.starterup.co.uk/user/login?_format=json', { name: mail, pass: 'randompass'}).subscribe((data:any) => {
    //     this.storage.set('uid', data.current_user.uid);
    //     this.router.navigateByUrl('/home');
    //    }, error => {
    //
    //      });


    //   (response) => {
    //   if (response.current_user) {
    //     alert(1);
    //   } else {
    //     alert(2);
    //   }
    // });
  }
}
