import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage} from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instructions-experimenter',
  templateUrl: './instructions-experimenter.page.html',
  styleUrls: ['./instructions-experimenter.page.scss'],
})
export class InstructionsExperimenterPage implements OnInit {

  constructor(private storage: Storage, private menuCtrl: MenuController, private router: Router) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.menuCtrl.enable(true, 'loggedIn');
      } else {
        this.menuCtrl.enable(true, 'loggedOut');
      }
    });
  }

  subjectInstructionsButtonClick() {
    this.router.navigateByUrl('/instructions-subject');
  }

  homeButtonClick() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.router.navigateByUrl('/home');
      } else {
        this.router.navigateByUrl('/login');
      }
    });
  }

}
