import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { ModalController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

declare var cordova;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  secs: number;
  constructor(
    private router: Router,
    public modalController: ModalController,
    public menuCtrl: MenuController,
    private storage: Storage
  ) {
  }
  ngOnInit(){
  }
  ionViewDidEnter() {
    this.storage.get('settinginput').then((val) => {
      console.log('settinginput '+val);
    });
    this.menuCtrl.enable(true, 'loggedIn');
  }

  startExperiment() {
    this.router.navigateByUrl('/waiting');
  }
  goSettings() {
    this.router.navigateByUrl('/settings');
  }
  listExperiments() {
    this.router.navigateByUrl('/list');
  }
}
