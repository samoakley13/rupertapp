import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperimenteePage } from './experimentee.page';

describe('ExperimenteePage', () => {
  let component: ExperimenteePage;
  let fixture: ComponentFixture<ExperimenteePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperimenteePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperimenteePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
