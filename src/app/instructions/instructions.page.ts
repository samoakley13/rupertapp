import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage} from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.page.html',
  styleUrls: ['./instructions.page.scss'],
})
export class InstructionsPage implements OnInit {

  constructor(private storage: Storage, private menuCtrl: MenuController, private router: Router) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.menuCtrl.enable(true, 'loggedIn');
      } else {
        this.menuCtrl.enable(true, 'loggedOut');
      }
    });
  }

  experimenterInstructionsButtonClick() {
    this.router.navigateByUrl('/instructions-experimenter');
  }

  subjectInstructionsButtonClick() {
    this.router.navigateByUrl('/instructions-subject');
  }

  homeButtonClick() {
    this.router.navigateByUrl('/home');
  }
}
