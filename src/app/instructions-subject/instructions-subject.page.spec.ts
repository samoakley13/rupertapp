import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructionsSubjectPage } from './instructions-subject.page';

describe('InstructionsSubjectPage', () => {
  let component: InstructionsSubjectPage;
  let fixture: ComponentFixture<InstructionsSubjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructionsSubjectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructionsSubjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
