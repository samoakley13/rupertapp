import { Injectable } from  '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';
import { EnvService } from './env.service';
import { Storage } from  '@ionic/storage';
import { User } from  'src/app/models/user';
import { AuthResponse } from  './auth-response';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authSubject  =  new  BehaviorSubject(false);
  constructor(
    private  httpClient:  HttpClient,
    private  storage:  Storage,
    private router: Router,
    private env: EnvService) { }
  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }
  login(user: User): Observable<AuthResponse> {
    const body = {
       'grant_type': 'password',
       'client_id' : '3e794051-d489-47e4-b4e9-5a03e8e435d7',
       'client_secret': 'rupertsecret',
       'username': user.email,
       'password': 'randompass',
       'scope': ''
    };
    const myFormData = this.getFormData(body);
    return this.httpClient.post(this.env.API_URL +'oauth/token', myFormData).pipe(
      tap(async (res: any) => {
          await this.storage.set("uid", res.user_id);
          await this.storage.set("ACCESS_TOKEN", res.access_token);
          await this.storage.set("REFRESH_TOKEN", res.refresh_token);
          this.authSubject.next(true);
      }, error => {
        this.httpClient.post('https://rs.starterup.co.uk/user/register?_format=json', { field_name: { value: user.name }, name: { value: user.email }, pass: { value: 'randompass' }, mail: { value: user.email }}).subscribe((data:any)  => {
          this.storage.set('uid', data.uid[0].value);
          this.router.navigateByUrl('/instructions');
          }, error => {
           console.log(error);
         });
      })
    );
  }
  async logout() {
    await this.storage.remove("uid");
    await this.storage.remove("ACCESS_TOKEN");
    await this.storage.remove("REFRESH_TOKEN");
    this.authSubject.next(false);
  }
  isLoggedIn() {
    return this.authSubject.asObservable();
  }
}
