import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  feedback: any;
  input: any;
  val: any;
  constructor(private storage: Storage) { }

  ionViewDidEnter() {
    this.storage.get('settingfeedback').then((val) => {
      if (val == '1'){
        this.feedback = true;
      } else {
        this.feedback = false;
      }
    });
    this.storage.get('settinginput').then((val2) => {
      if (val2 == '1'){
        this.input = true;
      } else {
        this.input = false;
      }
    });
  }
  changeFeedback(bool) {
    if (!this.feedback) {
      console.log('setting feedback to 1');
      this.storage.set('settingfeedback', '1');
    } else {
      console.log('setting feedback to 0');
      this.storage.set('settingfeedback', '0');
    }
  }
  changeInput(bool) {
    if (!this.input) {
      console.log('setting input to 1');
      this.storage.set('settinginput', '1');
    } else {
      console.log('setting input to 0');
      this.storage.set('settinginput', '0');
    }
  }
  ngOnInit() {
  }

}
