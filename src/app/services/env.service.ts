import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  API_URL = 'https://rs.starterup.co.uk/';
  constructor() { }
}
