import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'experiment', loadChildren: './experiment/experiment.module#ExperimentPageModule' },
  { path: 'experimentee', loadChildren: './experimentee/experimentee.module#ExperimenteePageModule' },
  { path: 'list', loadChildren: './list/list.module#ListPageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'results', loadChildren: './results/results.module#ResultsPageModule' },
  { path: 'instructions', loadChildren: './instructions/instructions.module#InstructionsPageModule' },
  { path: 'waiting', loadChildren: './waiting/waiting.module#WaitingPageModule' },
  { path: 'instructions-experimenter', loadChildren: './instructions-experimenter/instructions-experimenter.module#InstructionsExperimenterPageModule' },
  { path: 'instructions-subject', loadChildren: './instructions-subject/instructions-subject.module#InstructionsSubjectPageModule' },
  { path: 'results-past', loadChildren: './results-past/results-past.module#ResultsPastPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
