import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsPastPage } from './results-past.page';

describe('ResultsPastPage', () => {
  let component: ResultsPastPage;
  let fixture: ComponentFixture<ResultsPastPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsPastPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsPastPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
