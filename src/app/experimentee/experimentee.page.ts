import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { AudioService } from '../services/audio.service';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { Platform } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-experimentee',
  templateUrl: './experimentee.page.html',
  styleUrls: ['./experimentee.page.scss'],
})
export class ExperimenteePage implements OnInit {
  uid: any;
  nid: any;
  result_id: any;
  instructions: String;
  secs: number;
  countertext: String;
  public counter: any;
  hideButtons: boolean;
  experimentCount: number;
  chosenFeedback: number;
  input: number;
  isenabled:boolean=true;
  awaitAnswer:boolean=false;
  experimentCompleted=false;
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private loadingController: LoadingController,
    private menuCtrl: MenuController,
    private nativeAudio: NativeAudio,
    private audioService: AudioService,
    private router: Router,
    private speechRecognition: SpeechRecognition,
    public platform: Platform,
    private vibration: Vibration
) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.router.navigateByUrl('/home');
    });
  }

  ngOnInit() {
    this.hideButtons = true;

    this.storage.get('settingfeedback').then((val2) => {
      this.chosenFeedback = val2;
    });
    this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    this.storage.get('result_id').then((val) => {
      this.result_id = val;
    });
    this.storage.get('nid').then((val) => {
      this.nid = val;
    });
  }
  ngAfterViewInit(){

  }
  ionViewDidEnter() {
    console.log('view entered');
    this.setTime('activate');
    this.menuCtrl.enable(true, 'loggedIn');
    this.storage.get('settinginput').then((val) => {
      this.input = val;
      if (val == 1 && this.platform.is('cordova') == true) {
        if(this.chosenFeedback == 1) {
          this.instructions = 'Once the experiment has begun, please say "Yes" or "No" depending on if you believe you are being watched. You will feel your handset vibrate if you have answered incorrectly.';
          } else {
            this.instructions = 'Once the experiment has begun, please say "Yes" or "No" depending on if you believe you are being watched.';
        }
      } else {
        if(this.chosenFeedback == 1) {
          this.instructions = 'Once the experiment has begun, please select whether or not you feel you are being watched using the buttons below. After answering, you will hear a tone to indicate whether or not you have answered correctly.';
          } else {
            this.instructions = 'Once the experiment has begun, please select whether or not you feel you are being watched using the buttons below.';
          }
      }
    });
    if (this.platform.is('cordova')) {
      this.nativeAudio.preloadSimple('correct', 'assets/correct.wav');
      this.nativeAudio.preloadSimple('incorrect', 'assets/incorrect.wav');
      this.nativeAudio.preloadSimple('make', 'assets/make.wav');
      this.nativeAudio.preloadSimple('ding', 'assets/ding.mp3');
    } else {
      this.audioService.preload('correct', 'assets/correct.wav');
      this.audioService.preload('incorrect', 'assets/incorrect.wav');
      this.audioService.preload('make', 'assets/make.wav');
      this.audioService.preload('ding', 'assets/ding.mp3');
    }
  }

  ionViewDidLeave() {
    if (this.platform.is('cordova')) {
      this.nativeAudio.unload('correct');
      this.nativeAudio.unload('incorrect');
      this.nativeAudio.unload('make');
      this.nativeAudio.unload('ding');
    }
    clearInterval(this.counter);
    if(this.experimentCompleted == false) {
      var testId = this.storage.get('nid').then((val) => {
        this.http.post('https://rs.starterup.co.uk/rupert/test/delete', { nid: val}).subscribe((data:any) => {
          //window.location.reload();
        })
      });
    }
  }

  setTime(then) {
    let startTimestamp = new Date().getTime();
    this.http.get('https://epoch.starterup.co.uk/').subscribe((data:any) => {
      let endTimestamp = new Date().getTime();
      let timeDiff = Math.floor((endTimestamp-startTimestamp)/2);
      let time: number = parseInt(data)+timeDiff;
      let millistogo: number = 1000-time%1000;
      setTimeout(() => {
          clearInterval(this.counter);
          if (then == 'activate') {
            console.log('activateexperiment');
            this.activateExperiment(time+millistogo);
          } else if (then == 'start') {
            console.log('startexperiment');
            this.startExperiment(time+millistogo,1);
          }
      },millistogo);
    }, error => {
      console.log(error);
       //can't get time, reload?
      });
  }

  activateExperiment(time) {
    this.experimentCount = 0;
      let secs = time%10;
      if (secs > 5) {
        secs = 10+(10-secs);
      } else {
        secs = 10-secs
      }
      secs = secs+3;
      this.counter = setInterval(() => {
        if (secs == 0) {
          this.storage.get('settinginput').then((val) => {
            if(val == true) {
              if (this.platform.is('cordova') == true) {
                this.hideButtons = true;
              } else {
                //alert('Microphone not supported in browser. Switching to button input.');
                this.hideButtons = false;
              }
            } else {
              this.hideButtons = false;
            }
            this.setTime('start');
          }, error => {
            console.log('Storage error: ' + error);
            this.setTime('start');
          });
          
          if (this.input != 1 || this.platform.is('cordova') == false) {
            if (this.platform.is('cordova')) {
              this.nativeAudio.play('ding');
            } else {
              this.audioService.play('ding');
            }
          }
          
          clearInterval(this.counter);
        } else {
          this.countertext = 'Experiment begins in '+secs+' seconds.';
          secs = secs-1;
        }
      },1000)
  }

  startExperiment(time,start) {
    this.storage.get('bools').then((val) => {
      let secs = time%10;
      secs = 5-secs;
      this.counter = setInterval(() => {
        if(this.awaitAnswer == false) {
          if (secs == 2) {
            /*if (this.platform.is('cordova')) {
              this.nativeAudio.play('make');
            } else {
              this.audioService.play('make');
            }*/
            this.countertext = 'Make your decision in '+secs+' seconds.';
            secs = secs-1;
          } else if (secs == 0 || start == 1) {
              this.isenabled = true;
              this.awaitAnswer = true;
              this.countertext = 'Make your decision!';
              //reset timer, if on an app start voice recognition, if voice setting prompt for voice
              if (this.input == 1 && this.platform.is('cordova') == true && this.experimentCount <= 9) { //if voice setting is selected AND device is not a browser, we set up mic listener
                console.log('mic section entered with count: ' + this.experimentCount);
                this.experimentCount++;
                if(this.experimentCount >= 10) {
                  clearInterval(this.counter);
                  console.log('interval cleared and still running');
                }
                this.speechRecognition.startListening()
                  .subscribe(
                    (matches: Array<string>) => {
                      console.log('Matches: ' + matches);
                      if (matches[0] == 'yes' || matches[0] == 'yep' || matches[0] == 'yeah') {
                        this.watched(null,1);
                      } else if (matches[0] == 'no' || matches[0] == 'nope' || matches[0] == 'nah') {
                        this.watched(null,0);
                      } else {
                        this.watched(null,2);  //THIS NEEDS TO BE IMPLEMENTED SERVER SIDE!!
                      }
                      console.log('checking experiment count to move on in mic: ' + this.experimentCount);
                      if (this.experimentCount >= 10) {
                        this.experimentCompleted = true;
                        console.log('exiting page from mic section');
                        this.router.navigateByUrl('/results');
                      }
                    },
                    (onerror) => console.log('speech recognition error:', onerror)
                  )
              } else { //non microphone handling here
                if (this.experimentCount >= 10) {
                  this.experimentCompleted = true;
                  console.log('exiting page from button section');
                  this.router.navigateByUrl('/results');
                }
                this.experimentCount++; //Maybe??? Or maybe move on to results here??
                console.log('button experiment count increase to: ' + this.experimentCount);
              }
              secs = -1;
              this.setTime('start');
          } else {
            this.countertext = 'Make your decision in '+secs+' seconds.';
            secs = secs-1;
          }
        }
      },1000);
    });
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("fired"));
  }

  backHome() {
    console.log('back button hit');
    this.router.navigateByUrl('/home');
  }

  //maybe turn these two functions in to one function with a separate argument for watched
  watched(form,watched) {
    this.isenabled = false;
    //disable buttons for 2 seconds so people can't spam press and break test
    this.showLoadingSpinner();
    this.http.post('https://rs.starterup.co.uk/rupert/test/watched', { nid: this.nid, uid:this.uid, result_id:this.result_id, watched: watched }).subscribe((data:any) => {
      console.log(data);
      if(data.finish == 1) {
        this.router.navigateByUrl('/results');
      } else {
          if (this.chosenFeedback == 1) {
            if (this.input == 1 && this.platform.is('cordova') == true) {
              if (data.correct != 1) {
                this.vibration.vibrate(250);
              }
            } else {
              if (data.correct == 1) {
                if (this.platform.is('cordova')) {
                  this.nativeAudio.play('correct');
                } else {
                  this.audioService.play('correct');
                }
              } else {
                if (this.platform.is('cordova')) {
                  this.nativeAudio.play('incorrect');
                } else {
                  this.audioService.play('incorrect');
                }
              }
            }
          }
      }
    }, error => {
      console.log(error);
    });
    this.awaitAnswer = false;
}

  private async showLoadingSpinner() {
    const loading = await this.loadingController.create({
      spinner: "bubbles",
      duration: 1000,
      message: 'Sending response...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
