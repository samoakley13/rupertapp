import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    public menuCtrl: MenuController,
    private router: Router,
    private storage: Storage,
    private androidPermissions: AndroidPermissions
  ) {
    platform.ready().then(() => {

         this.androidPermissions.requestPermissions(
           [
             this.androidPermissions.PERMISSION.CAMERA,
             this.androidPermissions.PERMISSION.CALL_PHONE,
             this.androidPermissions.PERMISSION.GET_ACCOUNTS,
             this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
             this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
             this.androidPermissions.PERMISSION.INTERNET,
             this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
             this.androidPermissions.PERMISSION.ACCESS_WIFI_STATE,
             this.androidPermissions.PERMISSION.CHANGE_WIFI_STATE,
             this.androidPermissions.PERMISSION.CHANGE_NETWORK_STATE,
             this.androidPermissions.PERMISSION.ACCESS_NETWORK_STATE,
           ]
         );
         this.initializeApp();
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.storage.get('settingfeedback').then((val) => {
        console.log('init settingfeedback' +val);
        if (val == null) {
          this.storage.set('settingfeedback',1);
        }
      });
      this.storage.get('settinginput').then((val) => {
        console.log('init settinginput' +val);

        if (val == null) {
          this.storage.set('settinginput',1);
        }
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.storage.get('uid').then((val) => {
        if (val == null) {
          this.router.navigateByUrl('/login');
        } else {
          this.router.navigateByUrl('/home');
        }
      });
    });
  }

  toggleMenu() {
    this.menuCtrl.get("loggedOut");
  }



  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    })
    alert.present();
  }

  signoutButtonClick() {
    this.storage.clear();
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.router.navigateByUrl('/login');
  }

  changePasswordButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    window.location.href = 'https://rs.starterup.co.uk/user/3/edit';
  }

  aboutButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.menuCtrl.enable(true);
    this.router.navigateByUrl('/about');
  }

  instructionsButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.menuCtrl.enable(true);
    this.router.navigateByUrl('/instructions');
  }

  resultsPastButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.menuCtrl.enable(true);
    this.router.navigateByUrl('/results-past');
  }

  homeButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.menuCtrl.enable(true);
    this.router.navigateByUrl('/home');
  }

  registerButtonClick() {
    this.menuCtrl.close();
    this.menuCtrl.enable(false);
    this.menuCtrl.enable(true);
    this.router.navigateByUrl('/register');
  }
}
