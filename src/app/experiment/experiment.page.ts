import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AudioService } from '../services/audio.service';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';

declare var cordova;



@Component({
  selector: 'app-experiment',
  templateUrl: './experiment.page.html',
  styleUrls: ['./experiment.page.scss'],
})

export class ExperimentPage implements OnInit {
  experimentCount: number = 0;
  experiment: any;
  bools: Array<number>;
  look: Array<Object> = ['Look away from subject', 'Look at subject'];
  instruction: any;
  secs: number;
  countertext: String;
  public counter:any;
  hideMe: boolean;
  answered: boolean = false;
  experimentStarted: boolean = false;
  experimentCompleted: boolean = false;
  public counter2: any;
  constructor(
    private camera: Camera,
    public menuCtrl: MenuController,
    private storage: Storage,
    private audio: AudioService,
    private router: Router,
    private http: HttpClient,
    private platform: Platform
  )
  {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.router.navigateByUrl('/home');
    });

    this.storage.get('bools').then((val) => {
      this.bools = val;
      console.log('Values: ', val);
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    //this.setTime('activate');
  }

  ionViewDidEnter() {
    this.hideMe = true;
    this.audio.preload('ding', 'assets/ding.mp3');
    this.menuCtrl.enable(true, 'loggedIn');
    this.setTime('activate');
  }

  ionViewDidLeave() {
    clearInterval(this.counter);
    if(this.experimentCompleted == false) {
      var testId = this.storage.get('nid').then((val) => {
        this.http.post('https://rs.starterup.co.uk/rupert/test/delete', { nid: val}).subscribe((data:any) => {
          //window.location.reload();
        })
      });
    }
  }

  backHome() {
    console.log('back button hit');
    this.router.navigateByUrl('/home');
  }

  setTime(then) {
    let startTimestamp = new Date().getTime();
    this.http.get('https://epoch.starterup.co.uk/').subscribe((data:any) => {
      let endTimestamp = new Date().getTime();
      let timeDiff = Math.floor((endTimestamp-startTimestamp)/2);
      let time: number = parseInt(data)+timeDiff;
      let millistogo: number = 1000-time%1000;

      setTimeout(() => {
        clearInterval(this.counter);
        if (then == 'activate') {
          this.activateExperiment(time+millistogo);
        } else if (then == 'start') {
          this.startExperiment(time+millistogo);
        }
      },millistogo);
    }, error => {
      console.log(error);
      //can't get time, reload?
    });
  }

  activateExperiment(time) {
    console.log('activate experiment');
    //this.experimentCount = -1;
      let secs = time%10;
      if (secs > 5) {
        secs = 10+(10-secs);
      } else {
        secs = 10-secs
      }
      this.counter = setInterval(() => {
        if (secs == 0) {
          console.log('secs = 0');
          clearInterval(this.counter);
          this.setTime('start');
          console.log('INSTRUCTIONS:');
          this.instruction = this.look[this.bools[0]];
          console.log(this.instruction);
          this.countertext = 'Experiment has begun! Each time you hear a ding there will be a new instruction. We are waiting for the participant to answer the previous answer, at which point you will be given a new instruction.';
        } else {
          this.countertext = 'Experiment begins in '+secs+' seconds.';
          secs = secs-1;
        }
      },1000)
  }
  startExperiment(time) {
    console.log('start experiment');
    let secs = time%10;
    secs = 10-secs;

    this.counter2 = setInterval(() => {
      this.storage.get('nid').then((val) => {
        console.log('nid: '+val+' count: '+this.experimentCount);
        this.http.post('https://rs.starterup.co.uk/rupert/test/checkanswered', { nid: val, thecount: this.experimentCount}).subscribe((data:any) => {
          console.log(data);
          console.log(data['done']);
          if (data['done'] == 'done') {
            this.experimentCount = this.experimentCount+1; //move to next question
            console.log('INSTRUCTIONS:');
            let newinst = this.bools[this.experimentCount]; //get new instruction from array
            this.instruction = this.look[newinst]; //allocate verbal response to numeric data
            console.log(this.experimentCount);
            console.log(this.look[newinst]);

            this.audio.play('ding');

          } else if (data['done'] == 'finished') {
            this.experimentCompleted = true;
            this.router.navigateByUrl('/results');
            clearInterval(this.counter2);
          }
        }, error => {
          console.log(error);
            //can't get time, reload?
          });
      });
    }, 1000);
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    // this.camera.getPicture(options).then((imageData) => {
    //  // imageData is either a base64 encoded string or a file URI
    //  // If it's base64 (DATA_URL):
    //  this.activateExperiment();
    //  this.hideMe = false;
    //  let base64Image = 'data:image/jpeg;base64,' + imageData;
    // }, (err) => {
    //   console.log(err);
    //   this.activateExperiment();
    //  // Handle error
    // });
  }
}
