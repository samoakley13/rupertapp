import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnDestroy } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

declare var cordova;


@Component({
  selector: 'app-waiting',
  templateUrl: 'waiting.page.html',
  styleUrls: ['waiting.page.scss'],
})
export class WaitingPage {
  public waiting:any;
  group: string;
  lat: any;
  lng: any;
  joined: boolean = false;
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private router: Router,
    private geolocation: Geolocation,
    public menuCtrl: MenuController,
    private camera: Camera,
    private filePath: FilePath,
    private platform: Platform
  ) {
  }

  ngOnInit(){
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'loggedIn');
    this.startExperiment();
  }

  ionViewDidLeave() {
    clearInterval(this.waiting);
    if (!this.joined) {
      var testId = this.storage.get('nid').then((val) => {
        this.http.post('https://rs.starterup.co.uk/rupert/test/delete', { nid: val}).subscribe((data:any) => {
          //window.location.reload();
        })
      });
    }
  }

  cancel(form){
    this.http.get('https://rs.starterup.co.uk/rupert/test/cancel').subscribe(data => {
      this.router.navigateByUrl('/home');
       }, error => {
        console.log(error);
      });
  }

  listUsers() {
    this.http.get('https://rs.starterup.co.uk/rupert/users/get').subscribe(data => {
        var stri = JSON.stringify(data);
        console.log(stri);
       }, error => {
        console.log(error);
      });
  }
  startExperiment() {

    this.storage.get('uid').then((val) => {
      if (val > 0) {
    this.storage.get('group').then((val2) => {
      this.group = val2;
      console.log('Group: ', this.group);

      var uid = val;
        var lat = 51.47;
        var lng = -2.2;
        //this.geolocation.getCurrentPosition().then((resp) => {
         //this.http.post('https://rs.starterup.co.uk/rupert/test/create', { uid: uid, lat: resp.coords.latitude, lng: resp.coords.longitude}).subscribe((data:any) => {
         this.http.post('https://rs.starterup.co.uk/rupert/test/create', { uid: uid, group: this.group, lat: lat, lng: lng}).subscribe((data:any) => {
            //success, waiting for participant
            //TODO This needs some sort of timeout.
            this.waiting = setInterval(() => {
                            this.checkExperiment(uid,data.bools)
                          }, 2500);
            this.storage.set('bools', data.bools);
            this.storage.set('nid', data.nid);
            this.storage.set('goto', 'experiment');
            //check every 2-3s if your test has a participant
            //if it does, load the test info into app
             console.log(data);
            }, error => {
              alert(error);
             console.log(error);
           });
          });
      /*  }).catch((error) => {
          alert('Error getting location. Please check your GPS is enabled.')
        });*/
      }
    });
  }
  checkExperiment(uid,bools) {
      console.log('checking to see if somebody has joined'+uid);
       this.http.post('https://rs.starterup.co.uk/rupert/test/check', { uid: uid  }).subscribe((data:any) => {
         console.log(data);
        //load whether somebody has joined
        console.log(data.data);
        if (data.data['experimentee']) { //somebody has joined
          clearInterval(this.waiting);
          this.joined = true;
          this.router.navigateByUrl('/experiment');
          this.storage.set('result_id', data.data['result_id']);
        }
        }, error => {
        //  alert('Error, no other participant joined/was able to join.')
          console.log(error);
       });
  }
  openCamera(sourceType: PictureSourceType) {
    var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
    };
    // this.camera.getPicture(options).then((imagePath) => {
    //   if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
    //          this.filePath.resolveNativePath(imagePath)
    //              .then(filePath => {
    //                  let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //                  let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
    //                  this.fileService.copyFileToLocalDir(correctPath, currentName, this.fileService.createFileName());
    //              });
    //      } else {
    //          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //          this.fileService.startUpload(correctPath);
    //          //this.fileService.copyFileToLocalDir(correctPath, currentName, this.fileService.createFileName());
    //      }
    // }, (err) => {
    //   console.log(err);
    //  // Handle error
    // });
  }
}
