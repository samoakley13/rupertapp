import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-results-past',
  templateUrl: './results-past.page.html',
  styleUrls: ['./results-past.page.scss'],
})
export class ResultsPastPage implements OnInit {
  
  public rows: Array<{ date: string, time: string, score: string, percentage: number }> = [];
  private maxQuantity: number = 5;
  dataObj: any[];

  constructor(private menuCtrl: MenuController, private router: Router, private http: HttpClient, private storage: Storage ) { 
  }

  ngOnInit() {
    //Need to get values from server here for all past scores related to that user.
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        var uid = val;
        console.log('uuid: ' + uid);
        this.http.post('https://rs.starterup.co.uk/rupert/test/resultspast', { uid: uid }).subscribe((data:any) => {
            //data has list of nearby experiments
            this.dataObj = data.data;
            console.log(Object.entries(data.data));
            this.dataObj.forEach(element => {
              var unixTime = element.date;
              var dateTime = new Date(unixTime * 1000);
              var day = dateTime.getDate();
              var month = dateTime.getMonth();
              var year = dateTime.getFullYear();
              var hours = dateTime.getHours();
              var minutes = "0" + dateTime.getMinutes();

              var date = day + "/" + month + "/" + year;
              var time = hours + ":" + minutes.substr(-2);

              var score = element.score;
              var percentage = (score / 10) * 100;
    
              this.rows.push({ date: date, time: time, score: score, percentage: percentage });
            });
            }, error => {
            console.log(error);
        });
      }
    });
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'loggedIn');
  }

  homeButtonClick() {
    this.router.navigateByUrl('/home');
  }

}
