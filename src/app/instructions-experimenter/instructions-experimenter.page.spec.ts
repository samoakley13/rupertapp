import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructionsExperimenterPage } from './instructions-experimenter.page';

describe('InstructionsExperimenterPage', () => {
  let component: InstructionsExperimenterPage;
  let fixture: ComponentFixture<InstructionsExperimenterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructionsExperimenterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructionsExperimenterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
