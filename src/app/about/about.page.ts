import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage} from '@ionic/storage';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(private storage: Storage, private menuCtrl: MenuController) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.menuCtrl.enable(true, 'loggedIn');
      } else {
        this.menuCtrl.enable(true, 'loggedOut');
      }
    });
  }
}
