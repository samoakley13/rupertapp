import { Storage } from '@ionic/storage';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExperimentPage } from './experiment.page';

const routes: Routes = [
  {
    path: '',
    component: ExperimentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExperimentPage]
})
export class ExperimentPageModule {
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private router: Router,
    private httpClient: HttpClient,
  ) {
  }
  ngOnInit(){
    this.storage.get('bools').then((val) => {
      var arr = ['Look', 'No Look'];
      console.log(val);
    });
  }
}
