export interface AuthResponse {
    user: {
        user_id: number,
        name: string,
        email: string,
        access_token: string,
        expires_in: number
    }
}
