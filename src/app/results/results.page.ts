import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {
  correct: 0;
  total: 0;
  percent: any;
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private router: Router,
    public menuCtrl: MenuController,
    private platform: Platform
  ) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.router.navigateByUrl('/home');
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    var group = "";
    this.storage.get('group').then((val) => {
      group = val;
    }, error => {
      group = null;
      console.log(error);
    });
    this.storage.get('result_id').then((val) => {
      console.log(val);
      this.http.post('https://rs.starterup.co.uk/rupert/test/results', { result_id: val, group: group  }).subscribe((data:any) => {
        this.correct = data.correct;
        this.total = data.total;
        this.percent = ((data.correct/data.total)*100).toFixed(2);
      });
    });
    this.menuCtrl.enable(true, 'loggedIn');
  }

  goAgainButtonClick() {
    this.router.navigateByUrl('/home');
  }

  resultsPastButtonClick() {
    this.router.navigateByUrl('/results-past');
  }

}
