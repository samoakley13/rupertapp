import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage} from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instructions-subject',
  templateUrl: './instructions-subject.page.html',
  styleUrls: ['./instructions-subject.page.scss'],
})
export class InstructionsSubjectPage implements OnInit {

  constructor(private storage: Storage, private menuCtrl: MenuController, private router: Router) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.menuCtrl.enable(true, 'loggedIn');
      } else {
        this.menuCtrl.enable(true, 'loggedOut');
      }
    });
  }

  experimenterInstructionsButtonClick() {
    this.router.navigateByUrl('/instructions-experimenter');
  }

  homeButtonClick() {
    this.storage.get('uid').then((val) => {
      if (val > 0) {
        this.router.navigateByUrl('/home');
      } else {
        this.router.navigateByUrl('/login');
      }
    });
  }

}
